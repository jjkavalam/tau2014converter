/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

/*
    File: parser_netlist.c

    Parser for the netlist file
*/
#define SILENCE_ERRORS
#define MMAP_BASED

#include "pins.h"
#include "parser_netlist.h"
#include "parser_celllib.h"
#include "objects.h"
#include "vect.h"
#include "hash.h"
#include "main.h"
#include "timeit.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <pthread.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>


#define SQR(x) x*x
#define MAX_THREADS 8
IIPList iipList, icpList, rat_known_node_List;
IOPList iopList, at_known_node_List, slew_known_node_List;
WPList wpList;
WTList wtList;
IList iList;
WList wList;

char *clockNode;
float clockTP;
Wire *clkNet;

Instance* vSource;
Instance* vSink;


//ParserThread parser_thread[MAX_NUM_PARSER_THREADS];
long time_strcmp;
// Barrier variable
pthread_barrier_t barr;

//int strcmp(const char* s1, const char *s2)
//{
//    #ifdef ENABLE_TIMEIT
//    TimeIt time_str;
//    timeit_start(&time_str);
//    #endif
//    int i = strcmp(s1, s2);
//    #ifdef ENABLE_TIMEIT
//    timeit_stop(&time_str);
//    time_strcmp += time_str.time_spent;
//    #endif
//    return i;
//}

void setTimingData(float t[8], float mean){
    int i=1; for(i=1; i<8; i++) t[i] = 0;
    t[0] = mean;
}

void print_PI_PO(){
//input <nodename>
int i;
for (i = 0; i < vSource->numOPins; i++) fprintf(delay_file, "input %s\n", vSource->opin[i]->nodename);
for (i = 0; i < vSink->numIPins; i++) fprintf(delay_file, "output %s\n", vSink->ipin[i]->nodename);
}
void initNetlist_p()
{
   // Each thread has its own record of the things it parsed.
    // This function joins all of them together
    initIIPList(&iipList);
    initIIPList(&icpList);
    initIOPList(&iopList);
    initWPList(&wpList);
    initWTList(&wtList);
    initIList(&iList);
    initWList(&wList);
    initIIPList(&rat_known_node_List);
    initIOPList(&at_known_node_List);
    initIOPList(&slew_known_node_List);


    // Create virtual source and sink instance
    // Any PI pin encountered is connected to the opins of this virtual source
    // Similarly output pins are connected to the ipins of virtual sink
    vSource = (Instance*)malloc(sizeof(Instance));
    vSource->celltype = VSOURCE;
    //vSource->ipin = (instanceIPin**)malloc(sizeof(instanceIPin*)*0);
    vSource->opin = (instanceOPin**)malloc(sizeof(instanceOPin*)*MAX_PIs);
    vSource->numIPins = 0;
    vSource->numOPins = 0;

    vSink = (Instance*)malloc(sizeof(Instance));
    vSink->celltype = VSINK;
    vSink->ipin = (instanceIPin**)malloc(sizeof(instanceIPin*)*MAX_POs);
    vSink->numIPins = 0;
    vSink->numOPins = 0;

}

/*
 *  Function: readNetlist()
 *
 *  Job of readNetlist() is simple.
 *
 *  Firstly, For each pin it encounters in the netlist file
 *  create a pin of the appropriate type and add it to the corresponding linked list.
 *
 *  Secondly, for each object - INSTANCE, WIRE (RCTREE) - it encounters create
 *  add them to the corresponding linked list.
 *  Also make the connections between the object and its pins
 *
 */

// Line end could be a new line or null
int locate_newline(char* in, int start)
{
    int i = start;

    while (1)
    {
        if (in[i] == '\n')
            return i;
        else if (in[i] == 0)
            return -1;
        i++;
    }
}

char *mygets(int num, char* in)
{
    static int i = 0;
    char *str;
    if(in[i] == '\0') return (char*)NULL;
    str = in + i;
    while(in[i]!='\n')
        i++;
    in[i] = '\0';
    i++;
    return str;
}

void readNetlist()
{
    //ParserThread *parser_thread = (ParserThread*) argument;

    // open log
    // char logfilename[] = "x-parser.log";
    // logfilename[0] = (char) (49 + parser_thread->threadid);
    //FILE *logfile = fopen(logfilename,"a");
    char seps[]   = " ,\t\n:";
    char *saveptr = NULL;
    char *saveptr_memblk = NULL;
    char *saveptr_wire;
    char *saveptr_wire2;

    char *token, *keyword, *nodename, *wirenodename;

    int i,j,k;
    int LineNum;

    P_CELL *libcell = NULL;
    float fval;

    Instance* INode = NULL;
    Wire* WNode;
    instanceIPin *IIPNode;
    instanceOPin *IOPNode;
    wirePort *WPNode;
    wireTap *WTNode;
    wireTap *temptap[MAX_TAPS];

    //int tempnumnodes;
    //int tempnumneighbours[1+MAX_TAPS+MAX_INTERNALNODES];
    //int tempneighbour[1+MAX_TAPS+MAX_INTERNALNODES][MAX_TAPS];
    //float tempres[1+MAX_TAPS+MAX_INTERNALNODES][MAX_TAPS];
    //float tempcap[1+MAX_TAPS+MAX_INTERNALNODES];
    //char wirenode[1+MAX_INTERNALNODES+MAX_TAPS][65];
    int resnode1, resnode2;
    int tempnumnodes = 0;
    int *tempnumneighbours = (int*)malloc(sizeof(int)*(1+MAX_TAPS+MAX_INTERNALNODES));
    int **tempneighbour = (int**)malloc(sizeof(int*)*(1+MAX_TAPS+MAX_INTERNALNODES));
    for ( i = 0; i < 1+MAX_TAPS+MAX_INTERNALNODES; i++) tempneighbour[i] = (int*)malloc(sizeof(int)*MAX_TAPS);
    float ** tempres = (float**)malloc(sizeof(float*)*(1+MAX_TAPS+MAX_INTERNALNODES));
    for ( i = 0; i < 1+MAX_TAPS+MAX_INTERNALNODES; i++) tempres[i] = (float*)malloc(sizeof(float)*MAX_TAPS);
    float *tempcap = (float*)malloc(sizeof(float)*(1+MAX_TAPS+MAX_INTERNALNODES));
    char** wirenode = (char**)malloc(sizeof(char*)*(1+MAX_INTERNALNODES+MAX_TAPS));
    i = 1 + MAX_TAPS + MAX_INTERNALNODES;
    for ( i = 0; i < 1+MAX_TAPS+MAX_INTERNALNODES; i++) wirenode[i] = (char*)malloc(sizeof(char)*65);


    int insideWireScope = 0;

    LineNum = 0;

    //fprintf(stderr,"\n Thread %d: Begin at %ld",parser_thread->threadid, parser_thread->bof);

    //char* memblk = &parser_thread->memblk[parser_thread->bof];
    //long memblk_size = parser_thread->memblk_size;


    //int line_start = parser_thread->bof;
    //int line_end = line_start;
    //int line_length = -1;
    //char line[5000];
    char wire_header[5000];
    int startWireScope = 0;

    FILE *fp = fopen(netlistFile, "r");
    if (fp == NULL) exit(EXIT_FAILURE);

    char *line = NULL;
    char line_tmp[5000];
    size_t len = 0;
    ssize_t  read;

    //while(1)
    // {
        //fpos = ftell(fp); // Record the position before reading this line
        //fprintf(logfile,"%s",line);
        // Read the line into line
        // Patch the previous line
        // hoi\nhello world
        // 0
        //line_start += line_length + 1;

        //if (line_start == parser_thread->eof + 1)
        //{
        //    //fprintf(stderr,"\n Thread %d: Done",parser_thread->threadid);
        //    // Going to exit
        //    if (insideWireScope == 1)
        //    {
        //        // Save the location
        //        parser_thread->skipped_wire_start = startWireScope;
        //        //fprintf(stderr,"To do wire starting at %d",startWireScope);
        //    }
        //    break;
        //}
        //else if (line_start > parser_thread->eof)
        //{
        //    fprintf(stderr,"\n Thread %d: Overreading the allocated region. start at %d",parser_thread->threadid, line_start);
        //    break;
        //}

        //if (parser_thread->read_just_a_wire == 1)
        //{
        //    memblk = memblk + line_length + 1;
        //    line_tmp = memblk;
        //}
        //else
        //{
        //    if (LineNum == 0)
        //        line_tmp = strtok_r( memblk, "\n", &saveptr_memblk );
        //    else
        //        line_tmp = strtok_r( NULL, "\n" , &saveptr_memblk );
        //}
        //if (line_tmp == NULL)
        //{
        //    fprintf(stderr,"\n Thread %d Parsing failure",parser_thread->threadid);
        //}
        //strcpy(line,line_tmp);
        //line_length = strlen(line);
        //line_end = line_start + line_length;

        //LineNum++;

        // // fprintf(stderr,"\n T %2d L %4d :%s (%d:%d:%d)",parser_thread->threadid, LineNum, line,line_start,line_length,line_end);
   	while ((read = getline(&line, &len, fp)) != -1) {

        	strcpy(line_tmp,line);
          /* Establish string and get the first token: */
        keyword = strtok_r( line, seps, &saveptr );

        //if ( ( strcmp(keyword,"wire") != 0 || strcmp(keyword,"res") != 0 || strcmp(keyword,"cap") != 0 ) &&  insideWireScope == 1 )
        if ( ( strcmp(keyword,"res") != 0 && strcmp(keyword,"cap") != 0 ) &&  insideWireScope == 1 )
        {
            // Wire scope ends
            insideWireScope = 0;

            // Close the wire itself
            // fprintf(stderr,"\nThread %d Closing wire \n\t%s\n\'%s'",parser_thread->threadid,wire_header,keyword);

            strtok_r( wire_header, seps, &saveptr_wire );
            WNode = (Wire*)malloc(sizeof(Wire));
            addWire ( WNode, &(wList) );

            WNode->numTaps = 0;

            // Port node
            nodename = strtok_r ( NULL, seps, &saveptr_wire );

            WPNode = (wirePort*)malloc(sizeof(wirePort));
            addwirePort ( WPNode, &(wpList) );

            strcpy ( WPNode->nodename, nodename );
            WPNode->wire = WNode;

            WNode->port = WPNode;

            // Tap nodes
            nodename = strtok_r ( NULL, seps, &saveptr_wire );

            assert(WNode->numTaps == 0);
            do
            {
                WTNode = (wireTap*)malloc(sizeof(wireTap));
                addwireTap ( WTNode, &(wtList) );

                strcpy ( WTNode->nodename, nodename );

                WTNode->wire = WNode;

                temptap[WNode->numTaps] = WTNode;

                WNode->numTaps++;

                nodename = strtok_r ( NULL, seps, &saveptr_wire );

            } while ( nodename != NULL );

            WNode->tap = (wireTap**)malloc(sizeof(wireTap*)*(WNode->numTaps));
            for ( i = 0; i < WNode->numTaps; i++ )
            {
                WNode->tap[i] = temptap[i];
            }

            // Close the wire rc tree
            WNode->rctree.numnodes = tempnumnodes;
            WNode->rctree.numneighbours = (int*)malloc(sizeof(int)*tempnumnodes);
            WNode->rctree.neighbour = (int**)malloc(sizeof(int*)*tempnumnodes);
            WNode->rctree.cap = (float*)malloc(sizeof(float)*tempnumnodes);
            WNode->rctree.res = (float**)malloc(sizeof(float*)*tempnumnodes);

            for ( i = 0; i < tempnumnodes; i++ )
            {
                WNode->rctree.numneighbours[i] = tempnumneighbours[i];
                WNode->rctree.cap[i] = tempcap[i];
                WNode->rctree.neighbour[i] = (int*)malloc(sizeof(int)*tempnumneighbours[i]);
                WNode->rctree.res[i] = (float*)malloc(sizeof(float)*tempnumneighbours[i]);

                for ( j = 0; j < tempnumneighbours[i]; j++ )
                {
                    WNode->rctree.neighbour[i][j] = tempneighbour[i][j];
                    WNode->rctree.res[i][j] = tempres[i][j];
                }
            }

            //if (parser_thread->read_just_a_wire == 1)
            //{
            //    //fprintf(stderr,"\n Thread %d Finished reading the skipped wire. Break.",parser_thread->threadid);
            //    break;
            //}
        }
        if ( strcmp(keyword,"input") == 0 )
        {
            nodename = strtok_r ( NULL, seps, &saveptr );

            IOPNode = (instanceOPin*)malloc(sizeof(instanceOPin));
            addinstanceOPin ( IOPNode, &(iopList) );

            strcpy ( IOPNode->nodename , nodename );
            IOPNode->pinid = vSource->numOPins;
            IOPNode->instance = vSource;

            vSource->opin[vSource->numOPins] = IOPNode;
            vSource->numOPins++;
        }
        else if ( strcmp(keyword,"output") == 0 )
        {
            nodename = strtok_r ( NULL, seps, &saveptr );

            IIPNode = (instanceIPin*)malloc(sizeof(instanceIPin));
            addinstanceIPin ( IIPNode, &(iipList) );

            strcpy ( IIPNode->nodename , nodename );
            IIPNode->pinid = vSink->numIPins;
            IIPNode->instance = vSink;

            vSink->ipin[vSink->numIPins] = IIPNode;
            vSink->numIPins++;
        }
        else if ( strcmp(keyword,"instance") == 0 )
        {
            token = strtok_r ( NULL, seps, &saveptr );
            if ( token == NULL )
            {
                fprintf( stderr, "\n** Error in Line %d: Cell type not given.", LineNum );
            }
            for ( i = 0; i < p_numcells; i++ )
            {
                if ( strcmp ( token, p_cell[i].name ) == 0 )
                {
                    break;
                }
            }
            if ( i < p_numcells )
            {
                libcell = &p_cell[i];

                INode = (Instance*)malloc(sizeof(Instance));
                addInstance ( INode , &(iList) );

                INode->celltype = i;
		INode->samecellid = ++libcell->instance_count;
                INode->ipin = (instanceIPin**)malloc(sizeof(instanceIPin*)*libcell->numipins);
                INode->opin = (instanceOPin**)malloc(sizeof(instanceOPin*)*libcell->numopins);
                INode->numIPins = 0;
                INode->numOPins = 0;

            }
            else
            {
                fprintf ( stderr, "\n** Error in Line %d: Cell type %s not found in the cell library. ",LineNum, token );
            }

            for ( i = 0; i < libcell->numipins + libcell->numopins + libcell->hasclkpin ; i++ )
            {
                token = strtok_r ( NULL, seps ,&saveptr );
                //fprintf(main_logfile, "%s ", token);
                if ( token != NULL )
                {
                	if ( strcmp (token, libcell->clkpinname) == 0)
					{

                        nodename = strtok_r ( NULL, seps , &saveptr);

                        IIPNode = (instanceIPin*)malloc(sizeof(instanceIPin));
                        strcpy ( IIPNode->nodename , nodename );
                        IIPNode->pinid = -1; // clock! will be easy to debug
                        IIPNode->isclock = 1;
                        IIPNode->instance = INode;
                        addinstanceIPin ( IIPNode, &(icpList) );

                        INode->cpin = IIPNode;
					}
                    else
                    {
                        for ( j = 0; j < libcell->numipins; j++ )
                        {
                            if ( strcmp ( token, libcell->ipinname[j] ) == 0 )
                            {
                                break;
                            }
                        }
                        if ( j < libcell->numipins )
                        {
                            nodename = strtok_r ( NULL, seps ,&saveptr );

                            IIPNode = (instanceIPin*)malloc(sizeof(instanceIPin));
                            strcpy ( IIPNode->nodename , nodename );
                            IIPNode->pinid = j;
                            IIPNode->isclock = 0;
                            IIPNode->instance = INode;
                            addinstanceIPin ( IIPNode, &(iipList) );

                            INode->ipin[INode->numIPins] = IIPNode;
                            INode->numIPins++;

                        }
                        else
                        {
                            // Search among output pins
                            for ( j = 0; j < libcell->numopins; j++ )
                            {
                                if ( strcmp ( token, libcell->opinname[j] ) == 0 )
                                {
                                    break;
                                }
                            }
                            if ( j < libcell->numopins )
                            {
                                nodename = strtok_r ( NULL, seps, &saveptr );
                                IOPNode = (instanceOPin*)malloc(sizeof(instanceOPin));
                                strcpy ( IOPNode->nodename , nodename );
                                IOPNode->pinid = j;
                                IOPNode->instance = INode;
                                addinstanceOPin ( IOPNode, &(iopList) );

                                INode->opin[INode->numOPins] = IOPNode;
                                INode->numOPins++;

                            }
                            else
                            {
                                fprintf (stderr, "\n ** Error in Line %d. %s didn't match any of pins of cell %s.", LineNum, token, libcell->name );
                            }
                        }
                        }
                }
                else
                {
                    #ifndef SILENCE_ERRORS
                    fprintf ( stderr, "\n ** Error in Line %d. Cell library specifies atleast %d input pins. Fewer are specified.", LineNum, libcell->numipins );
                    #endif
                }
            }


            if ( strtok_r ( NULL , seps, &saveptr ) != NULL )
            {
                fprintf ( stderr, "\n** Warning in Line %d: More pins specified than those present for cell type %s ", LineNum , libcell->name );
            }
        }
        else if ( strcmp(keyword,"wire") == 0 )
        {
            //fprintf(stderr,"Line is **%s**",line_tmp);
            // Copy line_tmp for later use
            strcpy(wire_header,line_tmp);
            insideWireScope = 1;
            //startWireScope = line_start;
            // fprintf(stderr,"\nWire\n-----\n%s",line_tmp);
            nodename = strtok_r ( line_tmp, seps, &saveptr_wire2 );
            nodename = strtok_r ( NULL, seps, &saveptr_wire2 );
            strcpy ( wirenode[0], nodename );
            tempnumneighbours[0] = 0;

            // fprintf(stderr,"\nPort %s",nodename);
            tempnumnodes = 1;

            nodename = strtok_r ( NULL, seps, &saveptr_wire2 );
            do
            {
                strcpy ( wirenode[tempnumnodes], nodename );
                tempnumneighbours[tempnumnodes] = 0;

                tempnumnodes++;

                nodename = strtok_r ( NULL, seps, &saveptr_wire2 );
            } while ( nodename != NULL );
            //fprintf(stderr,"\nHas %d taps",tempnumnodes-1);
            // Initialize temporary RC tree structure

        }
        else if ( strcmp(keyword,"res") == 0 )
        {
            wirenodename = strtok_r ( NULL, seps, &saveptr );
            for ( i = 0; i < tempnumnodes; i++ )
            {
                if ( strcmp ( wirenodename , wirenode[i] ) == 0 )
                {
                    break;
                }
            }
            if ( i < tempnumnodes )
            {
                resnode1 = i;
            }
            else
            {
                tempnumnodes ++ ;
                tempnumneighbours[tempnumnodes-1] = 0;
                // fprintf(stderr,"\nRes node1 %s is being added as %d",wirenodename,tempnumnodes-1);

                strcpy ( wirenode[tempnumnodes-1] , wirenodename );
                resnode1 = tempnumnodes - 1;
            }

            wirenodename = strtok_r ( NULL, seps, &saveptr );
            for ( i = 0; i < tempnumnodes; i++ )
            {
                if ( strcmp ( wirenodename , wirenode[i] ) == 0 )
                {
                    break;
                }
            }
            if ( i < tempnumnodes )
            {
                resnode2 = i;
            }
            else
            {
                tempnumnodes ++ ;
                tempnumneighbours[tempnumnodes-1] = 0;
                // fprintf(stderr,"\nRes node2 %s is being added as %d",wirenodename,tempnumnodes-1);

                strcpy ( wirenode[tempnumnodes-1] , wirenodename );
                resnode2 = tempnumnodes - 1;
            }

            token = strtok_r ( NULL, seps, &saveptr );
            sscanf ( token, "%f", &fval );

            tempnumneighbours[resnode1]++;
            tempnumneighbours[resnode2]++;
            tempneighbour[resnode1][tempnumneighbours[resnode1]-1] = resnode2;
            tempneighbour[resnode2][tempnumneighbours[resnode2]-1] = resnode1;
            tempres[resnode1][tempnumneighbours[resnode1]-1] = fval;
            tempres[resnode2][tempnumneighbours[resnode2]-1] = fval;

        }
        else if ( strcmp(keyword,"cap") == 0 )
        {
            wirenodename = strtok_r ( NULL, seps, &saveptr );
            for ( i = 0; i < tempnumnodes; i++ )
            {
                if ( strcmp ( wirenodename , wirenode[i] ) == 0 )
                {
                    break;
                }
            }
            if ( i == tempnumnodes )
            {
                tempnumnodes ++ ;
                tempnumneighbours[tempnumnodes-1] = 0;
                // fprintf(stderr,"\nCap node %s is being added as %d",wirenodename,tempnumnodes-1);

                strcpy ( wirenode[tempnumnodes-1] , wirenodename );
            }

            token = strtok_r ( NULL, seps, &saveptr );
            sscanf ( token, "%f", &fval );
            tempcap[i] = fval;

        }
        else if ( strcmp(keyword,"at") == 0 )
        {
            nodename = strtok_r( NULL, seps, &saveptr);

            IOPNode = (instanceOPin*)malloc(sizeof(instanceOPin));
            addinstanceOPin ( IOPNode, &(at_known_node_List) );
            strcpy ( IOPNode->nodename , nodename );

            token = strtok_r(NULL, "", &saveptr);
            float value;
            sscanf( token, "%f", &value); setTimingData(IOPNode->tData.atFallEarly, value);
            sscanf( token, "%f", &value); setTimingData(IOPNode->tData.atFallLate, value);
            sscanf( token, "%f", &value); setTimingData(IOPNode->tData.atRiseEarly, value);
            sscanf( token, "%f", &value); setTimingData(IOPNode->tData.atRiseLate, value);
        }
        else if ( strcmp(keyword,"rat") == 0 )
        {
            nodename = strtok_r( NULL, seps, &saveptr);

            IIPNode = (instanceIPin*)malloc(sizeof(instanceIPin));
            addinstanceIPin ( IIPNode, &(rat_known_node_List) );
            strcpy ( IIPNode->nodename , nodename );

            //IIPNode->tData.slack = 1;
            token = strtok_r(NULL, seps, &saveptr);
            float value;
            if(strcmp(token, "early")==0)
            {
                token = strtok_r(NULL, seps, &saveptr); sscanf( token, "%f", &value);
                setTimingData(IIPNode->tData.ratFallEarly, value);
                token = strtok_r(NULL, seps, &saveptr); sscanf( token, "%f", &value);
                setTimingData(IIPNode->tData.ratRiseEarly, value);
                IIPNode->tData.slack_early = 1;
            }
            else if (strcmp(token, "late")==0)
            {
                token = strtok_r(NULL, seps, &saveptr); sscanf( token, "%f", &value);
                setTimingData(IIPNode->tData.ratFallLate, value);
                token = strtok_r(NULL, seps, &saveptr); sscanf( token, "%f", &value);
                setTimingData(IIPNode->tData.ratRiseLate, value);
                IIPNode->tData.slack_late = 1;
            }
            else
            {
                fprintf(stderr, "\n Error: Keyword rat is not followed by either of early or late.");
            }
        }
        else if ( strcmp(keyword,"slew") == 0 )
        {

            nodename = strtok_r( NULL, seps, &saveptr);

            IOPNode = (instanceOPin*)malloc(sizeof(instanceOPin));
            addinstanceOPin ( IOPNode, &(slew_known_node_List) );
            strcpy ( IOPNode->nodename , nodename );


            float value;
            token = strtok_r(NULL, seps, &saveptr);
            sscanf( token, "%f", &value);
            setTimingData(IOPNode->tData.slewFallEarly, value);
            setTimingData(IOPNode->tData.slewFallLate, value);

            token = strtok_r(NULL, seps, &saveptr);
            sscanf( token, "%f", &value);
            setTimingData(IOPNode->tData.slewRiseEarly, value);
            setTimingData(IOPNode->tData.slewRiseLate, value);
        }
        else if ( strcmp(keyword, "clock") == 0)
        {
            clockNode = strtok_r( NULL, seps, &saveptr);
            token = strtok_r(NULL, seps, &saveptr);
            sscanf( token, "%f", &clockTP);
            //fprintf(stderr,"Clock node is %s", clockNode);
        }
        else
        {
            fprintf ( stderr, "\n** Error in Line %d: Unknown keyword %s", LineNum, keyword );
        }

    }
    //fprintf(stderr, "\nThread %d: Number of Instances processed : %d \n", parser_thread->threadid, parser_thread->iList.count);

    //fclose(logfile);
	free(line);

    return;

}

/*
 *  Job of makeLinks() is to match pins across pin types using the nodename.
 *  i.e. for each pin in IIPList the WTList is checked to discover wire tap -> gate input connections
 *  Then, pointers of either nodes are set to the other.
 *
 *  Note that, the for a IIP node to link to a WT top, the IIP struct definition should provide for
 *  a pointer of WT* type.
 *
 *  Of course, by definition, a pin connects to exactly one other pin.
 *
 *  Improvements
 *  ------------
 *  To make string matching faster hash tables are used.
 *  First, the '101' hash function (see experiments/stringHash)
 *  is used to generate a hash value for each node(name). The hash values are maintained in a linear array.
 *  A search query then becomes searching for a number.
 *
 */

void createHashtables()
{
    #ifdef ENABLE_TIMEIT
        TimeIt time_createiopHash;
        timeit_start(&time_createiopHash);
    #endif
    createiopHash();
    #ifdef ENABLE_TIMEIT
        timeit_stop(&time_createiopHash);
        fprintf(perf_logfile, "\n\t createiopHash: %ld", time_createiopHash.time_spent);
    #endif

    #ifdef ENABLE_TIMEIT
        TimeIt time_createiipHash;
        timeit_start(&time_createiipHash);
    #endif
    createiipHash();
    #ifdef ENABLE_TIMEIT
        timeit_stop(&time_createiipHash);
        fprintf(perf_logfile, "\n\t createiipHash: %ld", time_createiipHash.time_spent);
    #endif

    #ifdef ENABLE_TIMEIT
        TimeIt time_createwpHash;
        timeit_start(&time_createwpHash);
    #endif
    createwpHash();
    #ifdef ENABLE_TIMEIT
        timeit_stop(&time_createwpHash);
        fprintf(perf_logfile, "\n\t createwpHash: %ld", time_createwpHash.time_spent);
    #endif

    #ifdef ENABLE_TIMEIT
        TimeIt time_createwtHash;
        timeit_start(&time_createwtHash);
    #endif
    createwtHash();
    #ifdef ENABLE_TIMEIT
        timeit_stop(&time_createwtHash);
        fprintf(perf_logfile, "\n\t createwtHash: %ld", time_createwtHash.time_spent);
    #endif

}


void makeLinks()
{
    // Traverse the linked list of instance output pins
    // and try to match with wire port
    char *nodename;
    unsigned int key;

    instanceIPin *IIPNode;
    instanceOPin *IOPNode;

    wirePort *WPNode;
    wireTap *WTNode;

    // IOP -> WP link

    IOPNode = iopList.head;
    while(IOPNode != NULL)
    {
        nodename = IOPNode->nodename;
        IOPNode->instance->numFanOut = 0;

        WPNode = searchWPNode (nodename);
        if ( WPNode != NULL )
        {
            WPNode->iop = IOPNode;
            IOPNode->wp = WPNode;
            IOPNode->instance->numFanOut += WPNode->wire->numTaps;
            //fprintf (stderr,"\n IOP =%s=> WP ",nodename);
        }
        else
        {
            /* Direct Connection from Output to input */
            IOPNode->wp = NULL;

            // Search for IOP -> IIP link
            IIPNode = searchIIPNode (nodename);
            if ( IIPNode != NULL )
            {
                IOPNode->instance->numFanOut += 1;
                if (IOPNode->iip != NULL)
                {
                    // This is a limitation of the current setup!
                    //fprintf( stderr, "\n*Warning: A gate output pin supports utmost one direct connection to another gate. The existing connection will get removed.");
                }

                IOPNode->iip = IIPNode;
                IIPNode->iop = IOPNode;
                //fprintf (stderr,"\n IOP =%s=> IIP ",nodename);
            }
            else
            {
                IOPNode->iip = NULL;
                //fprintf ( stderr, "\nUnconnected IOP found. nodename %s ", nodename);
            }
        }

       IOPNode = IOPNode->next;
    }


    // IIP -> WT link
    IIPNode = iipList.head;
    while(IIPNode != NULL)
    {
        nodename = IIPNode->nodename;

        WTNode = searchWTNode (nodename);

        if ( WTNode != NULL )
        {
            WTNode->iip = IIPNode;
            IIPNode->wt = WTNode;
            //fprintf (stderr,"\n IIP <=%s= WT %p",nodename, WTNode);
        }
        else
        {
            IIPNode->wt = NULL;
            //if(IIPNode->iop == NULL)
            //    fprintf ( stderr, "\nUnconnected IIP found. nodename %s ", nodename);
            //else fprintf( stderr, "\nFound input connected to output of another gate IOP =%s=> IIP", nodename);
        }

       IIPNode = IIPNode->next;
    }

    // clock -> WT Link
    IIPNode = icpList.head;
    while(IIPNode != NULL)
    {
        nodename = IIPNode->nodename;

        WTNode = searchWTNode (nodename);

        if ( WTNode != NULL )
        {
            WTNode->iip = IIPNode;
            IIPNode->wt = WTNode;
        }
        else
        {
            IIPNode->wt = NULL;
            if(IIPNode->iop == NULL)
                fprintf ( stderr, "\nUnconnected IIP found. nodename %s ", nodename);
            else fprintf( stderr, "\nFound input connected to\
                    output of another gate IOP =%s=> IIP", nodename);
        }

       IIPNode = IIPNode->next;
    }
}

/*
 *  Function: calculateDelaySlew
 *  Helper function to computeWireTime(), does the actual computation of the load capacitance, delay, slew values.
 *  Given the inputs, calculates the load capacitance seen by the driving gate, delay from port to tap(s),
 *  slew-of-impulse-response at each tap, all in parametric form.
 *
 *  Arguments:
 *  rctree - [input] pointer to RCTREE
 *  tapCap - [input] array of input pin capacitance connected to each tap node
 *  numtaps - [input] number of taps
 *  delayR - [output] array of (nominal , metal sensitivity) value pairs associated with the delay at each tap node
 *  slewR - [output] array of (nominal , metal sensitivity) value pairs associated with the output slew of the impulse response at each tap node
 *  outCap - [output] (nominal, metal sensitivity) value pair associated with the load capacitance, CL, seen at the output pin of a gate driving the wire
 *
 */

//void calculateDelaySlew(RCTREE* rctree, float tapCap[], int numtaps, float delayR[][2], float slewR[][2], float outCap[2])
void calculateDelaySlew(RCTREE* rctree, float* tapCap, int numtaps, float** delayR, float** slewR, float* outCap)
{

    int i, run;
    int size = rctree->numnodes;

    float *downCap = (float*)malloc(sizeof(float)*size);
    float *delay = (float*)malloc(sizeof(float)*size);

    int *queue = (int*)malloc(sizeof(int)*size);
    int *visited = (int*)malloc(sizeof(int)*size);
    int curr, end;

    int node, neigh;

    /*
     *  SETUP QUEUE
     *
     *  Setup the queue for downstream capacitances calculation.
     *  For each parent node, starting from the port node, add all child nodes to the queue.
     */

    curr = 0;
    end = 1;

    queue[0] = 0;

    visited[0] = 1;
    for(i = 1; i < size; i++) visited[i] = 0;

    while(curr < size)
    {
        node = queue[curr];

        for(i=0; i < rctree->numneighbours[node]; i++)
        {
            neigh = rctree->neighbour[node][i];

            if(visited[neigh] == 0) // Process only child nodes
            {
                queue[end++] = neigh;
                visited[neigh] = 1;
            }
        }

        curr++;
    }

    float *delay_0sigma = (float*)malloc(sizeof(float)*size);
    float *delay_3sigma = (float*)malloc(sizeof(float)*size);
    float *moment_0sigma = (float*)malloc(sizeof(float)*size);
    float *moment_3sigma = (float*)malloc(sizeof(float)*size);

    float res_scale_factor = 1.0;

    for ( run = 0;  run < 4; run++)
    {

        /*
         *  DOWNSTREAM CAPACITANCE
         *
         *  Starting from the end of the existing queue,
         *  update the downstream capacitance of the parent node.
         */


        downCap[0] = 0.0; // Port node

        if ( run == 0 ) // Normal delay calculation at 0 sigma
        {
        downCap[0] = rctree->cap[0];
            for (i=1; i<=numtaps; i++ ) // Tap nodes
                downCap[i] = (rctree->cap[i] + tapCap[i-1]);

            for (;i<size;i++) // Other nodes
                downCap[i] = rctree->cap[i];
        }
        else if ( run == 1 ) // Normal delay calculation at 3 sigma
        {
        downCap[0] = p_metal3_capscalefactor*rctree->cap[0];
            for (i=1; i<=numtaps; i++ ) // Tap nodes
                downCap[i] = ((p_metal3_capscalefactor*rctree->cap[i]) + tapCap[i-1]);

            for (;i<size;i++) // Other nodes
                downCap[i] = (p_metal3_capscalefactor*rctree->cap[i]);
        }
        else if ( run == 2 ) // Moment calculation at 0 sigma
        {
            for (i=1; i<=numtaps; i++ ) // Tap nodes
                downCap[i] = delay_0sigma[i]*(rctree->cap[i] + tapCap[i-1]);

            for (;i<size;i++) // Other nodes
                downCap[i] = delay_0sigma[i]*(rctree->cap[i]);
        }
        else if ( run == 3 ) // Moment calculation at 3 sigma
        {
            for (i=1; i<=numtaps; i++ ) // Tap nodes
                downCap[i] = delay_3sigma[i]*((p_metal3_capscalefactor*rctree->cap[i]) + tapCap[i-1]);

            for (;i<size;i++) // Other nodes
                downCap[i] = delay_3sigma[i]*(p_metal3_capscalefactor*rctree->cap[i]);
        }

        curr = size - 1;

        while(curr >= 0)
        {
            node = queue[curr];

            for(i=0; i< rctree->numneighbours[node]; i++)
            {
                neigh = rctree->neighbour[node][i];
                if(visited[neigh] == 1)
                {
                    downCap[neigh] += downCap[node];
                }
            }
            visited[node] = 0;
            curr--;
        }

        /*
         *  ELMORE DELAY
         *
         *  For each parent node, starting from the port node,
         *  assign, delay at the child node = delay at parent + (downstream cap of child * R_child_parent)
         */

        if (run == 0 || run == 2)
        {
            res_scale_factor = 1.0;
        }
        else if (run == 1 || run == 3)
        {
            res_scale_factor = p_metal3_resscalefactor;
        }

        curr = 0;

        visited[curr] = 1;
        delay[curr] = 0.0;

        while(curr < size)
        {
            node = queue[curr];

            for(i=0; i<rctree->numneighbours[node]; i++)
            {
                neigh = rctree->neighbour[node][i];
                if(visited[neigh] == 0) // Process only child node
                {
                    delay[neigh] = delay[node] + (res_scale_factor*rctree->res[node][i])*downCap[neigh];
                    visited[neigh] = 1;
                }
            }
            curr++;
        }

        //  Save delay at the 0 sigma run
        if (run == 0)
        {
            memcpy (delay_0sigma, delay, sizeof(float)*size);
        }
        else if (run == 1)
        {
            memcpy (delay_3sigma, delay, sizeof(float)*size);
        }
        else if (run == 2)
        {
            memcpy (moment_0sigma, delay, sizeof(float)*size);
        }
        else if (run == 3)
        {
            memcpy (moment_3sigma, delay, sizeof(float)*size);
        }
    }

    ///////////////////////////// DELAY ////////////////////////////////////////

    for ( i = 0; i < numtaps; i++)
    {
        delayR[i][0] = delay_0sigma[i+1];
        delayR[i][1] = (delay_3sigma[i+1] - delayR[i][0])/3;
    }

    ///////////////////////////// so^ /////////////////////////////////////////
    // nominal output slew of the impulse response
    // so^ = sqrt(2*beta - d^2)

    float slewR_3sigma;

    for ( i = 0; i < numtaps; i++)
    {
        slewR[i][0] = sqrt(2*moment_0sigma[i+1] - delay_0sigma[i+1]*delay_0sigma[i+1]);
        slewR_3sigma = sqrt(2*moment_3sigma[i+1] - delay_3sigma[i+1]*delay_3sigma[i+1]);
        slewR[i][1] = (slewR_3sigma - slewR[i][0])/3;
    }

    /////////////////////////// outCap ////////////////////////////////////////
    outCap[0] = 0.0;
    for (i=0 ; i<size; i++) // RCTREE caps
        outCap[0] += rctree->cap[i];
    for ( i = 0; i<numtaps; i++ ) // Tap Caps
        outCap[0] +=  tapCap[i];

    float outCap_3sigma = 0.0;
    for (i = 0 ; i<size; i++) // RCTREE caps
        outCap_3sigma += p_metal3_capscalefactor*rctree->cap[i];
    for (i = 0; i<numtaps; i++ ) // Tap nodes
        outCap_3sigma += tapCap[i];

    outCap[1] = (outCap_3sigma - outCap[0])/3;

    free(queue);
    free(visited);
    free(downCap);
    free(delay);
    free(delay_0sigma);
    free(delay_3sigma);
    free(moment_0sigma);
    free(moment_3sigma);
}

/*
 *  Function: computeWireTime
 *  Given a wire, evaluates and updates the load capacitance seen by the driving gate, delay from port to tap(s),
 *  slew-of-impulse-response at each tap, all in parametric form.
 */
void computeWireTime(Wire *wireIn){

    int i;

    //  ipinfallcap (or ipinrisecap) is cell pin fall (or rise) capacitance at each tap.
    int numtaps = wireIn->numTaps;
    // fprintf ( stderr, "\nCompute Wire Port: %s", wireIn->port->nodename);

    #ifdef ENABLE_LOGGING
        fprintf ( main_logfile, "\n\nWire Port: %5s", wireIn->port->nodename);
        for(i=0; i<numtaps; i++)
        {
            fprintf ( main_logfile, " %5s ", wireIn->tap[i]->nodename);
        }
        fprintf ( main_logfile, "\n=============================");
    #endif

    float *ipinfallcap=(float*)malloc(numtaps*sizeof(float));
    float *ipinrisecap=(float*)malloc(numtaps*sizeof(float));
    P_CELL* tCell;

    for(i=0; i< numtaps; i++)
    {
        ipinfallcap[i] = 0;
        ipinrisecap[i] = 0;
        wireTap *wt = wireIn->tap[i];
        instanceIPin *ipin = wt->iip;

        if(ipin->instance->celltype!=VSOURCE && ipin->instance->celltype!=VSINK )
        {
            tCell = &p_cell[ipin->instance->celltype];
            if(ipin->isclock != 1)
            {
                ipinfallcap[i] = tCell->ipinfallcap[ipin->pinid];
                ipinrisecap[i] = tCell->ipinrisecap[ipin->pinid];
            }
            else
            {
                ipinfallcap[i] = tCell->clkpinfallcap;
                ipinrisecap[i] = tCell->clkpinrisecap;
            }
        }
    }
    /*
     *  outCap is the load capacitance, CL, seen at the output pin of a gate driving the wire.
     *  This is used in the delay, slew calculation for a combinational cell. [Eqns 23,24]
     *
     *  Here outCap[2] stores CL in parametric form.
     *  Each parameter is calculated as per [Section 3.2]
     *
     *  outCap[0] - sum of all capacitances in the RC tree including cell pin capacitances at the taps
     *  outCap[1] - finite differencing at 0sigma, 3sigma corners for metal
     *
     */
    float outCap[2];

    float **delay = (float**)malloc(sizeof(float*)*numtaps);
    float **slew = (float**)malloc(sizeof(float*)*numtaps);
    for (i = 0; i < numtaps; i++)
    {
        delay[i] = (float*)malloc(sizeof(float)*2);
        slew[i] = (float*)malloc(sizeof(float)*2);
    }

    RCTREE* rctree = &(wireIn->rctree);

    calculateDelaySlew(rctree, ipinfallcap, numtaps, delay, slew, outCap);

    /* Copy the load capacitance calculated to the output pin also */
    wireIn->port->iop->fallCap[0] = outCap[0];
    wireIn->port->iop->fallCap[1] = outCap[1];

    /* Copy the delay slew values to the wire */
    for(i=0; i<numtaps; i++)
    {
        wireIn->iotiming[i].falldelay[0] = delay[i][0];
        wireIn->iotiming[i].falldelay[1] = delay[i][1];
        wireIn->iotiming[i].fallslew[0] = slew[i][0];
        wireIn->iotiming[i].fallslew[1] = slew[i][1];
    }

    /* Calculate and assign, the load capacitance, wire delay, slew similarly for the rise case */

    calculateDelaySlew(rctree, ipinrisecap, numtaps, delay, slew, outCap);

    wireIn->port->iop->riseCap[0] = outCap[0];
    wireIn->port->iop->riseCap[1] = outCap[1];

    for(i=0; i<numtaps; i++)
    {
        wireIn->iotiming[i].risedelay[0] = delay[i][0];
        wireIn->iotiming[i].risedelay[1] = delay[i][1];
        wireIn->iotiming[i].riseslew[0] = slew[i][0];
        wireIn->iotiming[i].riseslew[1] = slew[i][1];
    }

    #ifdef ENABLE_LOGGING

        fprintf ( main_logfile, \
                 "\n\n tap \t\t risedelay \t\t\t\t\t\t riseslew \
                 \n------------------------------------------------------------------" \
        );
        for (i = 0; i < numtaps; i++)
        {
            fprintf ( main_logfile, \
                     "\n %2d \t\t %.4e %.4e \t\t %.4e %.4e", \
                     i, wireIn->iotiming[i].risedelay[0],wireIn->iotiming[i].risedelay[1],\
                    wireIn->iotiming[i].riseslew[0],wireIn->iotiming[i].riseslew[1] \
            );
        }

        fprintf ( main_logfile, \
                 "\n\n tap \t\t falldelay \t\t\t\t\t\t fallslew \
                 \n------------------------------------------------------------------" \
        );
        for (i = 0; i < numtaps; i++)
        {
            fprintf ( main_logfile, \
                     "\n %2d \t\t %.4e %.4e \t\t %.4e %.4e", \
                     i, wireIn->iotiming[i].falldelay[0],wireIn->iotiming[i].falldelay[1],\
                    wireIn->iotiming[i].fallslew[0],wireIn->iotiming[i].fallslew[1] \
            );
        }
    #endif

}

void* computeWireTData_p(void *args)
{
    int tid = *(int*)args;
    int chunk = (wList.size-1)/MAX_THREADS+1;
    int i, uplimit = ((tid+1)*chunk > wList.size) ? wList.size : (tid+1)*chunk;
    for(i = tid*chunk ; i < uplimit ; i++)
        computeWireTime(wList.ptr[i]);
}

void computeWireTData(){
    int i, rcc;
    pthread_t thr[MAX_THREADS];
    for(i = 0; i< MAX_THREADS; i++ )
        rcc = pthread_create(&thr[i], NULL, computeWireTData_p, (void *)&i);
    for(i = 0; i < MAX_THREADS; i++)
        rcc = pthread_join(thr[i], NULL);

}

void consolidateNetlist()
{
   // Here we assume that the rats, if specified, will be only for primary outputs
    // to search for them among the input pins of the sink node.
    // The netlist file will specify the late, early rats separately
    // Hence two nodes corresponding to the same node can be present in the rat_known_node list
    // In that case both are to be merged into one.
    // Merge at_known_node, slew_known_node data with the
    // iop pins
    instanceOPin *iopnode, *at_known_node, *slew_known_node;
    
        at_known_node = at_known_node_List.head;
        while (at_known_node != NULL )
        {
            int i;
            for(i=0; i< vSource->numOPins; i++)
            {
                iopnode = vSource->opin[i];
                if (strcmp (at_known_node->nodename, iopnode->nodename ) == 0 )
                {
                    // Merge the nodes
                    // copy timing data
                    memcpy( iopnode->tData.atFallEarly , at_known_node->tData.atFallEarly, 8*sizeof(float));
                    memcpy( iopnode->tData.atFallLate , at_known_node->tData.atFallLate, 8*sizeof(float));
                    memcpy( iopnode->tData.atRiseEarly , at_known_node->tData.atRiseEarly, 8*sizeof(float));
                    memcpy( iopnode->tData.atRiseLate , at_known_node->tData.atRiseLate, 8*sizeof(float));

                }
                iopnode = iopnode->next;
            }

            at_known_node = at_known_node->next;
        }

        slew_known_node = slew_known_node_List.head;
        // fprintf(stderr, "Slew Nodes list has %d elements \n", parser_thread[i].slew_known_node_List.count);
        while (slew_known_node != NULL )
        {
            iopnode = iopList.head;
            while (iopnode != NULL)
            {
                if (strcmp (slew_known_node->nodename, iopnode->nodename ) == 0 )
                {
                    // Merge the nodes
                    // copy timing data
                    memcpy( iopnode->tData.slewFallEarly , slew_known_node->tData.slewFallEarly, 8*sizeof(float));
                    memcpy( iopnode->tData.slewFallLate , slew_known_node->tData.slewFallLate, 8*sizeof(float));
                    memcpy( iopnode->tData.slewRiseEarly , slew_known_node->tData.slewRiseEarly, 8*sizeof(float));
                    memcpy( iopnode->tData.slewRiseLate , slew_known_node->tData.slewRiseLate, 8*sizeof(float));

                }
                iopnode = iopnode->next;
            }

            slew_known_node = slew_known_node->next;
        }

	instanceIPin *iipnode;
   instanceIPin *rat_known_node = rat_known_node_List.head;
int i;

    while (rat_known_node != NULL)
    {
        for ( i = 0; i < vSink->numIPins; i++ )
        {
            iipnode = vSink->ipin[i];
            if (strcmp (rat_known_node->nodename, iipnode->nodename ) == 0 )
            {
                assert (rat_known_node->tData.slack_late == 1 || rat_known_node->tData.slack_early == 1 );

                if (rat_known_node->tData.slack_early == 1)
                {
                    memcpy( iipnode->tData.ratFallEarly , rat_known_node->tData.ratFallEarly, 8*sizeof(float));
                    memcpy( iipnode->tData.ratRiseEarly , rat_known_node->tData.ratRiseEarly, 8*sizeof(float));
                    iipnode->tData.slack_early = 1;
                    //iipnode->tData.slack = 1; // Should be removed once support for early, late slack is provided
                }
                else if (rat_known_node->tData.slack_late == 1)
                {
                    memcpy( iipnode->tData.ratFallLate , rat_known_node->tData.ratFallLate, 8*sizeof(float));
                    memcpy( iipnode->tData.ratRiseLate , rat_known_node->tData.ratRiseLate, 8*sizeof(float));
                    iipnode->tData.slack_late = 1;
                    // iipnode->tData.slack = 1; // Should be removed once support for early, late slack is provided
                }

                break;
            }
        }
        assert(i < vSink->numIPins); // rat is specified for a node which is not a primary output

        rat_known_node = rat_known_node->next;
    }


    if (clockNode != NULL)
    {
        int itr = 0;
        for(itr; itr< wList.size; itr++)
        {
            if(strcmp(((Wire *)wList.ptr[itr])->port->nodename, clockNode)) break;
        }
        if(itr == wList.size) { fprintf(stderr, "Couldn't find the Clock Net");}
        else  clkNet = (Wire *)wList.ptr[itr];
    }
}
//void readNetlist()
//{
    // Assigns parts of the file that each thread is to read and
    // Spawns the parser threads

   // int rc, i;

   // struct stat sb;
   // if (stat (netlistFile, & sb) != 0) fprintf (stderr, "'stat' failed for '%s': %s.\n", netlistFile, strerror (errno));
   // //fprintf(stderr, "\nFile size = %ld",sb.st_size);

   // long chunk_size = sb.st_size/global_params.netlist_parser_num_threads;
   // //fprintf(stderr,"\nChunk size %ld ", chunk_size);

   // int fd = open(netlistFile, O_RDONLY);
   // #ifdef ENABLE_TIMEIT
   //     TimeIt time_mmap;
   //     timeit_start(&time_mmap);
   // #endif
   // char* memblk = mmap(NULL, sb.st_size, PROT_WRITE, MAP_PRIVATE, fd, 0);
   // long memblk_size = sb.st_size;
   // #ifdef ENABLE_TIMEIT
   //     timeit_stop(&time_mmap);
   //     fprintf(perf_logfile, "\nmmap : %ld", time_mmap.time_spent);
   // #endif
   // // fprintf(stderr,"\nReading %s ...\n", netlistFile);

   // // Allocate chunks
   // int lnl;
   // for (i=0; i<global_params.netlist_parser_num_threads; ++i)
   // {
   //     parser_thread[i].threadid = i;

   //     // Align to line boundaries
   //     if ( i == 0)
   //     {
   //         parser_thread[i].bof = 0;
   //     }
   //     else
   //     {
   //         parser_thread[i].bof = parser_thread[i-1].eof + 1;
   //     }

   //     lnl = locate_newline(memblk, parser_thread[i].bof+chunk_size);
   //     if (lnl > -1)
   //     {
   //        parser_thread[i].eof = lnl;
   //     }
   //     else
   //     {
   //         parser_thread[i].eof = memblk_size - 1;
   //         if ( i != global_params.netlist_parser_num_threads - 1 )
   //         {
   //             //fprintf(stderr, "\n EOF reached prematurely. To use only %d threads",i+1);
   //             global_params.netlist_parser_num_threads = i + 1;

   //             //fprintf(stderr, "\n Thread %2d S %10d (%d) E %10d (%d)",i,parser_thread[i].bof,(int) memblk[parser_thread[i].bof],parser_thread[i].eof,(int) memblk[parser_thread[i].eof]);
   //             break;
   //         }
   //     }

   //     //fprintf(stderr, "\n Thread %2d S %10d (%d) E %10d (%d)",i,parser_thread[i].bof,(int) memblk[parser_thread[i].bof],parser_thread[i].eof,(int) memblk[parser_thread[i].eof]);

   //     // Same for everyone
   //     parser_thread[i].skipped_wire_start = -1;
   //     parser_thread[i].memblk = memblk;
   //     parser_thread[i].memblk_size = memblk_size;
   //     parser_thread[i].read_just_a_wire = 0;
   // }

   // // create all threads
   // for (i=0; i<global_params.netlist_parser_num_threads; ++i)
   // {
   //    //fprintf(stderr,"In main: creating thread %d\n", i);
   //    rc = pthread_create(&parser_thread[i].thread, NULL, readNetlist_p, (ParserThread *) &parser_thread[i]);
   //    assert(0 == rc);
   // }

   // // wait for all threads to complete
   // for (i=0; i<global_params.netlist_parser_num_threads; ++i)
   // {
   //    rc = pthread_join(parser_thread[i].thread, NULL);
   //    assert(0 == rc);
   // }

   // // Now go and read the wires that were left out as thread0
   // for (i=0; i<global_params.netlist_parser_num_threads; ++i)
   // {
   //     if (parser_thread[i].skipped_wire_start != -1)
   //     {
   //         //fprintf(stderr, "\n Thread %d had skipped some wire. Reading it now.",i);
   //         parser_thread[i].read_just_a_wire = 1;
   //         parser_thread[i].bof = parser_thread[i].skipped_wire_start;
   //         parser_thread[i].eof = memblk_size - 1;
   //         readNetlist_p(&parser_thread[i]);
   //     }
   // }

    // vector *something = malloc(sizeof(vector));
    // initV(something,1);
    // pushbackV(v,char*)
    // vector->ptr[i];

    // parser_thread[0].threadid = 0;
    // parser_thread[0].eof = chunk_size;
    // parser_thread[0].bof = 0;
    // readNetlist_p((ParserThread *) &parser_thread[0]);
//}
void parseNetlist()
{
    // fprintf(stderr, "Parsing Netlist\n");

    #ifdef ENABLE_TIMEIT
        TimeIt time_initNetlist_p;
        timeit_start(&time_initNetlist_p);
    #endif
    initNetlist_p();
    #ifdef ENABLE_TIMEIT
        timeit_stop(&time_initNetlist_p);
        fprintf(perf_logfile, "\ninitNetlist_p: %ld", time_initNetlist_p.time_spent);
    #endif

    #ifdef ENABLE_TIMEIT
        TimeIt time_readNetlist;
        timeit_start(&time_readNetlist);
    #endif
    readNetlist();
    #ifdef ENABLE_TIMEIT
        timeit_stop(&time_readNetlist);
        fprintf(perf_logfile, "\nreadNetlist: %ld", time_readNetlist.time_spent);
    #endif

    #ifdef ENABLE_TIMEIT
        TimeIt time_consolidateNetlist;
        timeit_start(&time_consolidateNetlist);
    #endif
    consolidateNetlist();
    #ifdef ENABLE_TIMEIT
        timeit_stop(&time_consolidateNetlist);
        fprintf(perf_logfile, "\nconsolidateNetlist: %ld", time_consolidateNetlist.time_spent);
    #endif

    // fprintf(stderr, "\nNumber of instances read in = %d",iList.count);

    #ifdef ENABLE_TIMEIT
        TimeIt time_createHashtables  ;
        timeit_start(&time_createHashtables);
    #endif
    createHashtables();
    #ifdef ENABLE_TIMEIT
        timeit_stop(&time_createHashtables);
        fprintf(perf_logfile, "\ncreateHashtables: %ld", time_createHashtables.time_spent);
    #endif

    // fprintf(stderr, "\nFinished parsing... Now making Links....\n");
    #ifdef ENABLE_TIMEIT
        TimeIt time_makelinks;
        timeit_start(&time_makelinks);
    #endif
    makeLinks();
    #ifdef ENABLE_TIMEIT
        timeit_stop(&time_makelinks);
        fprintf(perf_logfile, "\nmakelinks: %ld", time_makelinks.time_spent);
    #endif
    // fprintf(stderr, "\nLinks Done.. Now processing Wire info....\n");

    #ifdef ENABLE_TIMEIT
        TimeIt time_compwire;
        timeit_start(&time_compwire);
    #endif
    computeWireTData();
    #ifdef ENABLE_TIMEIT
        timeit_stop(&time_compwire);
        fprintf(perf_logfile, "\ncompute wire: %ld", time_compwire.time_spent);
    #endif

    // fprintf(stderr, "\nWire Information Processed\n");
}

