/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "parser_celllib.h"
#include "parser_netlist.h"
#include "objects.h"
#include "pins.h"
#include "engine.h"
#include "output.h"
#include "timeit.h"
#include "main.h"

char *netlistFile;
char *cellFile;
char *outFile;
char *delayFile;
char **envVars;
int num_threads;

FILE *out_file;
FILE *delay_file;

#ifdef ENABLE_LOGGING
    FILE *main_logfile;
#endif
#ifdef ENABLE_TIMEIT
    FILE *perf_logfile;
#endif

GlobalParams global_params;

main(int argc, char **argv, char **envp){
    printf("SSTA Timer Team T13_11, IIT Madras\n");
    #ifdef ENABLE_TIMEIT
        TimeIt time_main;
        timeit_start(&time_main);
    #endif

    if (argc < 3)
    {
        printf("\n Usage: %s <cell library> <netlist> <delay file> \n\n",argv[0]);
        return;
    }

    // > Export the 3 essential command line arguments to global variables
    cellFile = argv[1];
    netlistFile = argv[2];
    outFile = argv[3];
    delayFile = argv[3];
    num_threads = 1;
    // fprintf(stderr, "Trying to run it with %d threads\n", num_threads);

    envVars = envp;

    //out_file = fopen ("out.log","w");
    //if (out_file == NULL)
    //{
    //    printf ("\n Unable to open file %s",outFile);
    //    exit(-1);
    //}

    delay_file = fopen (delayFile, "w");
    if (delay_file == NULL)
    {
        printf ("\n Unable to open file %s",delayFile);
        exit(-1);
    }

    //fprintf ( out_file, "\n === New Run ===\n");

    // > Open the log file if logging is enabled.
    #ifdef ENABLE_TIMEIT
        perf_logfile = fopen("runtime.log","a");
        if (perf_logfile == NULL )
        {
            fprintf(stdout,"\n Unable to open perf log file.");
        }
        fprintf(perf_logfile,"\n=== New Run ===> %s \n",global_params.debug_message);
        fprintf(perf_logfile, "Benchmark : %s Num_threads : %s\n", argv[2], argv[4]);
    #endif

    #ifdef ENABLE_LOGGING
        main_logfile = fopen("T13_11_output.log","a");
        if (main_logfile == NULL )
        {
            fprintf(stdout,"\n Unable to open logging file.");
        }
    #endif

    // > Global parameters

    // >> Set the default values
    global_params.netlist_parser_num_threads = 1;

    // >> Allow override the parameters using command line arguments
    #ifdef SET_GLOBAL_PARAMS
    {
        process_args(argc, argv);
    }
    #endif

    // >> Perform checks on the parameter values
    int checks_ok = 1;
    if (global_params.netlist_parser_num_threads > MAX_NUM_PARSER_THREADS || global_params.netlist_parser_num_threads < 1)
    {
        printf("\nNumber of parser threads should be in the range 1 to %d\n", MAX_NUM_PARSER_THREADS);
        checks_ok = 0;
    }
    if ( checks_ok == 0 )
    {
        printf("\nBye!\n\n");
        exit(-1);
    }

    #ifdef ENABLE_LOGGING
    {
        // Write to the

        printf("%s",global_params.debug_message);
        fprintf(main_logfile,"\n=== New Run ===> %s \n",global_params.debug_message);
        int i;
        for ( i = 0; i < argc; i++ )
        {
            fprintf(main_logfile," %s",argv[i]);
        }

        // >> Display the global parameters
        fprintf(main_logfile,"\n ==== Global Parameters ===== ");
        fprintf(main_logfile,"\n  * netlist_parser_num_threads = %d",global_params.netlist_parser_num_threads);
        fprintf(main_logfile,"\n ============================ \n");
    }
    #endif


    // fprintf(stderr, "%s",global_params.debug_message);
    // fprintf(stderr,"\n=== New Run ===> %s \n",global_params.debug_message);
    int i;
    // for ( i = 0; i < argc; i++ )
    // {
    //     fprintf(stderr," %s",argv[i]);
    // }

    // >> Display the global parameters
    // fprintf(stderr,"\n ==== Global Parameters ===== ");
    // fprintf(stderr,"\n  * netlist_parser_num_threads = %d",global_params.netlist_parser_num_threads);
    // fprintf(stderr,"\n ============================ \n");


    #ifdef ENABLE_TIMEIT
        TimeIt time_parseCellLibrary;
        timeit_start(&time_parseCellLibrary);
    #endif
    parseCellLibrary();
    #ifdef ENABLE_TIMEIT
        timeit_stop(&time_parseCellLibrary);
        fprintf(perf_logfile,"\nparseCellLibrary = %ld", time_parseCellLibrary.time_spent);
    #endif

    #ifdef ENABLE_LOGGING
        fprintf(main_logfile,"\nSummary");
        fprintf(main_logfile,"\n-------");
        fprintf(main_logfile,"\nNumber of cells = %d\n",p_numcells);
    #endif

    #ifdef ENABLE_TIMEIT
        TimeIt time_parseNetlist;
        timeit_start(&time_parseNetlist);
    #endif
    parseNetlist();
    #ifdef ENABLE_TIMEIT
        timeit_stop(&time_parseNetlist);
        fprintf(perf_logfile,"\nparseNetlist = %ld", time_parseNetlist.time_spent);

    #endif

    print_PI_PO();

    #ifdef ENABLE_TIMEIT
        TimeIt time_timingAnalysis;
        timeit_start(&time_timingAnalysis);
    #endif
    timingAnalysis();
    #ifdef ENABLE_TIMEIT
        timeit_stop(&time_timingAnalysis);
        fprintf(perf_logfile,"\ntimingAnalysis = %ld", time_timingAnalysis.time_spent);
    #endif

    #ifdef ENABLE_TIMEIT
        TimeIt time_toolOutput;
        timeit_start(&time_toolOutput);
    #endif
    //toolOutput();
    #ifdef ENABLE_TIMEIT
        timeit_stop(&time_toolOutput);
        fprintf(perf_logfile,"\ntoolOutput = %ld", time_toolOutput.time_spent);
    #endif


    #ifdef ENABLE_TIMEIT
        timeit_stop(&time_main);
        fprintf(perf_logfile,"\nmain = %ld",time_main.time_spent);
    #endif

    #ifdef ENABLE_LOGGING
        fclose(main_logfile);
    #endif
    #ifdef ENABLE_TIMEIT
        fclose(perf_logfile);
    #endif

    //fclose(out_file);
    fclose(delay_file);
    fprintf(stderr, "Complete!\n");
//    freeWList(&WList);
//    freeIList(&IList);
}

