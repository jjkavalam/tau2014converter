/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#include <stdio.h>
#include <stdlib.h>

#include "timeit.h"

float fib(int n)
{    
    if ( n == 0 )
        return 0;
    else if ( n == 1 )
        return 1;
    else
        return fib(n-1) + fib(n-2);
}

int main(int argc, char* argv[])
{    
    TimeIt timeit, timeit2;
    int i;
    
    timeit_start(&timeit2);
    for ( i = 0; i < 5; i++)
    {
        timeit_start(&timeit);
        printf("\n Fibonacci 40 = %f ",fib(40));
        timeit_stop(&timeit);
        printf("\n Time spent = %ld \n",timeit.time_spent);
    }
    timeit_stop(&timeit2);
    printf("\n Total time spent = %ld \n",timeit2.time_spent);    
    return;
}
