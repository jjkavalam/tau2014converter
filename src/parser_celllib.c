/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#include "parser_celllib.h"
#include "main.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

float p_metal0_resscalefactor;
float p_metal0_capscalefactor;
float p_metal3_resscalefactor;
float p_metal3_capscalefactor;

int p_numcells;

P_CELL *p_cell;


void initCellLibrary(){
    // Initialize
    p_numcells = 0;
    // Allocate memory for 50 P_CELL pointers
    p_cell = (P_CELL*)malloc(sizeof(P_CELL)*MAX_CELLS);
}


void readCellLibrary()
{

    char seps[]   = " ,\t\n";
    char *token, *keyword;
    char line[BUFFERSIZE];

    FILE *fp;
    if((fp=fopen(cellFile, "r"))==NULL) {
        #ifdef ENABLE_LOGGING
            //fprintf(main_logfile,"Error in opening %s file \n", cellFile);
        #endif
        exit(1);
    }
    #ifdef ENABLE_LOGGING
        //fprintf(main_logfile,"Reading %s ...\n", cellFile);
    #endif

    int i, j, k;
    int NumTokens;
    int LineNum = 0;
    int keywordid;
    float fval;
    int iscotiming = 0;
    int ival, ival2 = 0;
    char sval[100];

    #define CCELL p_cell[p_numcells-1]
    while(1){
        // Read the line into line
        if (fgets(line, BUFFERSIZE, fp) == NULL) break;
        LineNum++;

        /* Establish string and get the first token: */
        keyword = strtok( line, seps );
        if (keyword != NULL){
            if ( strcmp(keyword,"metal") == 0 )
            {
                keywordid = 0;
                NumTokens = 3;
            }
            else if ( strcmp(keyword,"cell") == 0 )
            {
                keywordid = 1;
                NumTokens = 1;
            }
            else if ( strcmp(keyword,"pin") == 0 )
            {
                keywordid = 2;
                NumTokens = 1;  // Handled differently
            }
            else if ( strcmp(keyword,"timing") == 0 )
            {
                keywordid = 3;
                NumTokens = 39;
            }
            else if ( strcmp(keyword,"setup") == 0 )
            {
                keywordid = 4;
                NumTokens = 9;
            }
            else if ( strcmp(keyword,"hold") == 0 )
            {
                keywordid = 5;
                NumTokens = 9;
            }
            else
            {
                #ifdef ENABLE_LOGGING
                    //fprintf(main_logfile,"\n **Error on Line %d : keyword %s is unknown.",LineNum,keyword);
                #endif
                keywordid = 0;
                NumTokens = 0;
            }

            for (i = 0; i < NumTokens;i++){
               token = strtok( NULL, seps );
               if (token != NULL){
                    if (keywordid == 0){
                        // metal
                        switch (i){
                            case 0:
                                sscanf( token, "%d", &ival);
                                break;
                            case 1:
                                sscanf( token, "%f", &fval);
                                if (ival == 0){
                                    p_metal0_resscalefactor = fval;
                                }
                                if (ival == 3){
                                    p_metal3_resscalefactor = fval;
                                }
                                break;
                            case 2:
                                sscanf( token, "%f", &fval);
                                if (ival == 0){
                                    p_metal0_capscalefactor = fval;
                                }
                                if (ival == 3){
                                    p_metal3_capscalefactor = fval;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    if ( keywordid == 1 ){
                        // cell
                        switch (i){
                            case 0:
                                p_numcells++;
                                if (p_numcells > MAX_CELLS){
                                    #ifdef ENABLE_LOGGING
                                        //fprintf(main_logfile,"\n **Fatal Error on Line %d : Number of cells cannot exceed %d ",LineNum,MAX_CELLS);
                                    #endif
                                    return;
                                }
                                strcpy(CCELL.name,token);
                                // Initialize
				CCELL.instance_count = 0;
                                CCELL.hasclkpin = 0;
                                CCELL.numipins = 0;
                                CCELL.numopins = 0;
                                for ( j = 0; j < MAX_IPINS; j++ ){
                                    CCELL.setup[j].hasdata = 0;
                                    CCELL.hold[j].hasdata = 0;
                                    for ( k = 0; k < MAX_OPINS; k++){
                                        CCELL.iotiming[j][k].hasdata = 0;
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    if ( keywordid == 2 ){
                        // pin token is handled differently
                        strcpy(sval,token);
                        // Get next token
                        token = strtok( NULL, seps );
                        if (token != NULL){
                            if ( strcmp( token, "input" ) == 0 )
                            {
                                // input pin
                                CCELL.numipins++;
                                strcpy ( CCELL.ipinname[CCELL.numipins-1], sval );
                                // read two more floats
                                token = strtok( NULL, seps );
                                if (token != NULL){
                                    sscanf ( token, "%f", &fval);
                                    CCELL.ipinfallcap[CCELL.numipins-1] = fval;
                                }
                                else
                                {
                                    #ifdef ENABLE_LOGGING
                                        //fprintf(main_logfile,"\n **Error on Line %d : Insufficient data for input pin ",LineNum);
                                    #endif
                                }
                                token = strtok( NULL, seps );
                                if (token != NULL){
                                    sscanf ( token, "%f", &fval);
                                    CCELL.ipinrisecap[CCELL.numipins-1] = fval;
                                }
                                else
                                {
                                    #ifdef ENABLE_LOGGING
                                        //fprintf(main_logfile,"\n **Error on Line %d : Insufficient data for input pin ",LineNum);
                                    #endif
                                }
                            }
                            else if ( strcmp( token, "clock" ) == 0 )
                            {
                                CCELL.hasclkpin = 1;
                                strcpy ( CCELL.clkpinname, sval );

                                // read two more floats
                                token = strtok( NULL, seps );
                                if (token != NULL){
                                    sscanf ( token, "%f", &fval);
                                    CCELL.clkpinfallcap = fval;
                                }
                                else
                                {
                                    #ifdef ENABLE_LOGGING
                                        //fprintf(main_logfile,"\n **Error on Line %d : Insufficient data for clock pin ",LineNum);
                                    #endif
                                }
                                token = strtok( NULL, seps );
                                if (token != NULL){
                                    sscanf ( token, "%f", &fval);
                                    CCELL.clkpinrisecap = fval;
                                }
                                else
                                {
                                    #ifdef ENABLE_LOGGING
                                        //fprintf(main_logfile,"\n **Error on Line %d : Insufficient data for clock pin ",LineNum);
                                    #endif
                                }

                            }
                            else if ( strcmp( token, "output" ) == 0 )
                            {
                                // output pin
                                CCELL.numopins++;
                                strcpy ( CCELL.opinname[CCELL.numopins-1], sval );
                            }
                            else
                            {
                                #ifdef ENABLE_LOGGING
                                    //fprintf(main_logfile,"\n **Error on Line %d : Unknown pin type %s.",LineNum, token);
                                #endif
                            }
                        }
                        else
                        {
                            #ifdef ENABLE_LOGGING
                                //fprintf(main_logfile,"\n **Error on Line %d : Expected atleast 2 tokens following keyword pin. Found only 1.",LineNum);
                            #endif
                        }
                    }
                    if ( keywordid == 3 ){
                        // timing
                        if ( i == 0 ){
                            // Needs to determine if timing is io or co
                            // Check if the pin is a valid input pin and if so find
                            // the pin index
                            for ( j = 0; j < CCELL.numipins; j++){
                                if ( strcmp( token, CCELL.ipinname[j] ) == 0 ){
                                    break;
                                }
                            }
                            if ( j < CCELL.numipins)
                            {
                                // valid ipin
                                ival = j;
                                iscotiming = 0;
                            }
                            else
                            {
                                // check if its a valid clk pin
                                if ( strcmp( CCELL.clkpinname, token ) == 0 )
                                {
                                    iscotiming = 1;
                                }
                                else
                                {
                                    #ifdef ENABLE_LOGGING
                                        //fprintf(main_logfile,"\n **Error on Line %d : Invalid input/clk pin.",LineNum);
                                    #endif
                                    break;
                                }
                            }

                        }
                        else if ( i == 1 )
                        {
                            // check if pin is a valid output pin
                            for ( j = 0; j < CCELL.numopins; j++){
                                if ( strcmp( token, CCELL.opinname[j] ) == 0 ){
                                    break;
                                }
                            }
                            if ( j < CCELL.numopins)
                            {
                                // valid opin
                                ival2 = j;
                            }
                            else
                            {
                                #ifdef ENABLE_LOGGING
                                    //fprintf(main_logfile,"\n **Error on Line %d : Invalid output pin.",LineNum);
                                #endif
                                break;
                            }
                        }
                        else if ( i == 2 ){
                            if (iscotiming == 0){

                                CCELL.iotiming[ival][ival2].hasdata = 1;

                                if ( strcmp( token, "positive_unate") == 0 )
                                {
                                    CCELL.iotiming[ival][ival2].sense = POSITIVE_UNATE;
                                }
                                else if ( strcmp( token, "negative_unate") == 0 )
                                {
                                    CCELL.iotiming[ival][ival2].sense = NEGATIVE_UNATE;
                                }
                                else if ( strcmp( token, "non_unate") == 0 )
                                {
                                    CCELL.iotiming[ival][ival2].sense = NON_UNATE;
                                }
                                else
                                {
                                    #ifdef ENABLE_LOGGING
                                        //fprintf(main_logfile,"\n **Error on Line %d : Unknown timing sense.",LineNum);
                                    #endif
                                    break;
                                }
                            }
                            else
                            {
                                CCELL.cotiming[ival2].hasdata = 1;

                                if ( strcmp( token, "positive_unate") == 0 )
                                {
                                    CCELL.cotiming[ival2].sense = POSITIVE_UNATE;
                                }
                                else if ( strcmp( token, "negative_unate") == 0 )
                                {
                                    CCELL.cotiming[ival2].sense = NEGATIVE_UNATE;
                                }
                                else if ( strcmp( token, "non_unate") == 0 )
                                {
                                    CCELL.cotiming[ival2].sense = NON_UNATE;
                                }
                                else
                                {
                                    #ifdef ENABLE_LOGGING
                                        //fprintf(main_logfile,"\n **Error on Line %d : Unknown timing sense.",LineNum);
                                    #endif
                                    break;
                                }

                            }
                        }
                        else if ( i >= 3 && i <= 11 ){

                            sscanf( token, "%f", &fval);

                            if (iscotiming == 0){
                                CCELL.iotiming[ival][ival2].fallslew[i-3] = fval;
                            }
                            else
                            {
                                CCELL.cotiming[ival2].fallslew[i-3] = fval;
                            }

                        }
                        else if ( i >= 12 && i <= 20 ){

                            sscanf( token, "%f", &fval);

                            if (iscotiming == 0){
                                CCELL.iotiming[ival][ival2].riseslew[i-12] = fval;
                            }
                            else
                            {
                                CCELL.cotiming[ival2].riseslew[i-12] = fval;
                            }

                        }
                        else if ( i >= 21 && i <= 29 ){

                            sscanf( token, "%f", &fval);

                            if (iscotiming == 0){
                                CCELL.iotiming[ival][ival2].falldelay[i-21] = fval;
                            }
                            else
                            {
                                CCELL.cotiming[ival2].falldelay[i-21] = fval;
                            }

                        }
                        else if ( i >= 30 && i <= 38 ){

                            sscanf( token, "%f", &fval);

                            if (iscotiming == 0){
                                CCELL.iotiming[ival][ival2].risedelay[i-30] = fval;
                            }
                            else
                            {
                                CCELL.cotiming[ival2].risedelay[i-30] = fval;
                            }

                        }
                        else
                        {
                            // Never occurs
                        }
                    }
                    if ( keywordid == 4 ){

                        // setup
                        if ( i == 0 )
                        {
                            // check if its a valid clk pin
                            if ( strcmp( CCELL.clkpinname, token ) != 0 )
                            {
                                #ifdef ENABLE_LOGGING
                                    //fprintf(main_logfile,"\n **Error on Line %d : Invalid clk pin.",LineNum);
                                #endif
                                break;
                            }
                        }
                        else if ( i == 1 )
                        {
                            // Check if the pin is a valid input pin and if so find
                            // the pin index
                            for ( j = 0; j < CCELL.numipins; j++){
                                if ( strcmp( token, CCELL.ipinname[j] ) == 0 ){
                                    break;
                                }
                            }
                            if ( j < CCELL.numipins)
                            {
                                // valid ipin
                                ival = j;
                            }
                            else
                            {
                                #ifdef ENABLE_LOGGING
                                    //fprintf(main_logfile,"\n **Error on Line %d : Invalid input pin.",LineNum);
                                #endif
                            }
                        }
                        else if ( i == 2 )
                        {

                            CCELL.setup[ival].hasdata = 1;

                            if ( strcmp( token, "falling") == 0 )
                            {
                                CCELL.setup[ival].edgetype = FALLING;
                            }
                            else if ( strcmp( token, "rising") == 0 )
                            {
                                CCELL.setup[ival].edgetype = RISING;
                            }
                            else
                            {
                                #ifdef ENABLE_LOGGING
                                    //fprintf(main_logfile,"\n **Error on Line %d : Unknown edge type.",LineNum);
                                #endif
                                break;
                            }

                        }
                        else if ( i >= 3 && i <= 5 )
                        {
                            sscanf( token, "%f", &fval);
                            CCELL.setup[ival].fall[i-3] = fval;
                        }
                        else if ( i >= 6 && i <= 8 )
                        {
                            sscanf( token, "%f", &fval);
                            CCELL.setup[ival].rise[i-6] = fval;
                        }
                        else
                        {
                            // Never occurs
                        }

                    }
                    if ( keywordid == 5 ){

                        // hold
                        if ( i == 0 )
                        {
                            // check if its a valid clk pin
                            if ( strcmp( CCELL.clkpinname, token ) != 0 )
                            {
                                #ifdef ENABLE_LOGGING
                                    //fprintf(main_logfile,"\n **Error on Line %d : Invalid clk pin.",LineNum);
                                #endif
                                break;
                            }
                        }
                        else if ( i == 1 )
                        {
                            // Check if the pin is a valid input pin and if so find
                            // the pin index
                            for ( j = 0; j < CCELL.numipins; j++){
                                if ( strcmp( token, CCELL.ipinname[j] ) == 0 ){
                                    break;
                                }
                            }
                            if ( j < CCELL.numipins)
                            {
                                // valid ipin
                                ival = j;
                            }
                            else
                            {
                                #ifdef ENABLE_LOGGING
                                    //fprintf(main_logfile,"\n **Error on Line %d : Invalid input pin.",LineNum);
                                #endif
                            }
                        }
                        else if ( i == 2 )
                        {

                            CCELL.hold[ival].hasdata = 1;

                            if ( strcmp( token, "falling") == 0 )
                            {
                                CCELL.hold[ival].edgetype = FALLING;
                            }
                            else if ( strcmp( token, "rising") == 0 )
                            {
                                CCELL.hold[ival].edgetype = RISING;
                            }
                            else
                            {
                                #ifdef ENABLE_LOGGING
                                    //fprintf(main_logfile,"\n **Error on Line %d : Unknown edge type.",LineNum);
                                #endif
                                break;
                            }

                        }
                        else if ( i >= 3 && i <= 5 )
                        {
                            sscanf( token, "%f", &fval);
                            CCELL.hold[ival].fall[i-3] = fval;
                        }
                        else if ( i >= 6 && i <= 8 )
                        {
                            sscanf( token, "%f", &fval);
                            CCELL.hold[ival].rise[i-6] = fval;
                        }
                        else
                        {
                            // Never occurs
                        }
                    }
               }
               else
               {
                    #ifdef ENABLE_LOGGING
                        //fprintf(main_logfile,"\n **Error on Line %d : Expected %d tokens following keyword %s. Found only %d.",LineNum,NumTokens,keyword,i);
                    #endif
                    break;
               }
            }
            if (strtok( NULL , seps ) != NULL){
                #ifdef ENABLE_LOGGING
                    //fprintf(main_logfile,"\n **Warning on Line %d : Expected only %d tokens following keyword %s. More left.",LineNum,NumTokens,keyword);
                #endif
                while(strtok( NULL , seps ) != NULL);
            }
        }

    }


}

void parseCellLibrary(){
    initCellLibrary();
    readCellLibrary();

    #ifdef ENABLE_LOGGING
        //fprintf(main_logfile,"\nParsing complete. Bye!\n");
    #endif
}

