/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#include <stdio.h>
#include <stdlib.h>

#include "readwholefile.h"

#include "timeit.h"

#define BUFFER_SIZE 100

size_t bytes_read;
unsigned char * file_contents;

void all_at_once(char *fname)
{
    read_whole_file (fname, &file_contents, &bytes_read);
    
    char line[BUFFER_SIZE];

    while(1)
    {
        if (myfgets(line, BUFFER_SIZE) == NULL) break;
        //printf("\n%s",line);
    }
    
    free (file_contents);
}
void line_by_line(char* fname)
{
    char line[BUFFER_SIZE];
    
    FILE *fp=fopen(fname, "r");
    
    while(1)
    {
        if (fgets(line, BUFFER_SIZE, fp) == NULL) break;
    }
}

/*
void showtime()
{
    char buffer[30];
    struct timeval tv;    
    time_t curtime;
    gettimeofday(&tv, NULL); 
    curtime=tv.tv_sec;    
    strftime(buffer,30,"%m-%d-%Y  %T.",localtime(&curtime));
    printf("%s%ld\n",buffer,tv.tv_usec);
    
}
*/
int main(int argc, char* argv[])
{    
    if (argc < 2)
    {
        printf("\n Usage: %s <filename>\n",argv[0]);
        return 0;
    }

    TimeIt timeit1, timeit2;

    timeit_start(&timeit2);
    line_by_line(argv[1]);
    timeit_stop(&timeit2);
    printf("\n Line by line, Time spent = %ld",timeit2.time_spent);

    timeit_start(&timeit1);                          
    all_at_once(argv[1]);                           
    timeit_stop(&timeit1);                           
    printf("\n All at once, Time spent = %ld",timeit1.time_spent);
    
    printf("\n Bye!");
    return 0;
}
    
