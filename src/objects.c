/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#include "objects.h"
#include <stdlib.h>

void addInstance (Instance *node, IList *list)
{
   node->next = list->head;
   list->head = node;
   list->count++;

}

void initIList (IList *list)
{
   list->head = NULL;
   list->count = 0;
}

void freeInstance (Instance *node)
{
   free (node);
}

void freeIList (IList *list)
{

   Instance *node;

   node = list->head;
   while(list->head != NULL)
   {
      list->head = node->next;
      freeInstance ( node );
      node = list->head;
   }

}

// Wire list
void addWire (Wire *node, WList *list)
{
    pushbackV(list, node);
}

void initWList (WList *list)
{
    initV(list, 1);
}

void freeWire (Wire *node)
{
   free (node);
}

void freeWList (WList *list)
{

   Wire *node;
   int i = 0;
   for(; i < list->size; i++) freeWire((Wire*)list->ptr[i]);
   free(list->ptr);
   free(list);

}
