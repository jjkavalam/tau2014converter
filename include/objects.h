/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#ifndef _OBJECTS_H_
#define _OBJECTS_H_

#include "parser_celllib.h"
#include "vect.h"
// for TIMING_DATA data structure
/*
 *  Objects are special groupings of pins that are meaningful to the computation to be performed.
 *  For example, an arrival time (which is a 'signal') can be propagated to an instance output 'pin'
 *  only if arrival times are available at all the input pins.
 *
 *  Hence it is meaningful to group all the input pins of a gate. This gives rise to an Instance.
 *
 *  Similarly, whenever, a signal propagates from the port pin of a wire its tap pin
 *  it also progates to all other tap pins. This gives rise to a Wire.
 *
 *  An object, being a collection of pins, should posses the following essential feature:
 *  Direct access to all its pins.
 *
 *  In this design, we store part of the transfer function definition of the pin with the containers.
 */

struct _Instance
{
    int numIPins, numVisIn;
    int numOPins, numVisOut, numFanOut;

    struct _instanceIPin **ipin;
    struct _instanceOPin **opin;
    struct _instanceIPin *cpin;

    int samecellid, celltype;

    struct _Instance *next;
};
typedef struct _Instance Instance;

// Special instances
// These are not added the instance lists
#define MAX_PIs 300
extern Instance* vSource;
#define MAX_POs 300
extern Instance* vSink;

// Wire and RC Tree
#define MAX_TAPS 1000
#define MAX_INTERNALNODES 10000

typedef struct {
    int numnodes;
    int *numneighbours;
    int **neighbour;
    float **res;
    float *cap;
} RCTREE;

struct _Wire
{
    // numPorts = 1
    int numTaps;

    struct _wirePort *port;
    struct _wireTap **tap;

    RCTREE rctree;
    WIRE_TIMING_DATA iotiming[MAX_TAPS];

    struct _Wire *next;
};
typedef struct _Wire Wire;


/// Linked Lists ////

typedef struct
{
    Instance *curr, *head;
    int count;

} IList;

void initIList (IList *list);
void freeIList (IList *list);
void addInstance (Instance *node, IList *list);

/*  typedef struct
{
    Wire *curr, *head;
    int count;

} WList;
*/
typedef vector WList;

void initWList (WList *list);
void freeWList (WList *list);
void addWire (Wire *node, WList *list);

extern IList iList;
extern WList wList;

#endif
