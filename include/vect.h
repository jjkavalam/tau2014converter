/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#ifndef __VEC__
#define __VEC__

typedef struct
{
    void **ptr;                                 /* contains array of pointers to the vector */
    int size, capacity;                         /* size gives the size of array capacity gives storage strength */
}vector;


void initV(vector *, int );
void pushbackV(vector *, void *);
void destroy_vect(vector *);
void* popbackV(vector *);
void* popV(vector *);
void addV(vector *, vector *);
void destroy_vect(vector *v);
#endif
