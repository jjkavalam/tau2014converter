/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

/*
 *  Changelog
 */
#ifndef _PARSER_CELLLIB_H_
#define _PARSER_CELLLIB_H_


#define BUFFERSIZE 500
#define STRINGLEN 30

/* The cell library parser */

#define MAX_CELLS 105
#define MAX_IPINS 100
#define MAX_OPINS 200
#define MAX_TREE_NODES 100

// Timing sense
#define POSITIVE_UNATE 0
#define NEGATIVE_UNATE 1
#define NON_UNATE 2
// Edge type
#define FALLING 0
#define RISING 1

extern float p_metal0_resscalefactor;
extern float p_metal0_capscalefactor;
extern float p_metal3_resscalefactor;
extern float p_metal3_capscalefactor;

extern int p_numcells;


typedef struct{
    int hasdata;
    int sense;
    float fallslew[9];
    float riseslew[9];
    float falldelay[9];
    float risedelay[9];
}TIMING_DATA;

typedef struct{
	/* mean value and metal dependence */
	float falldelay[2];
	float risedelay[2];
	float fallslew[2];
	float riseslew[2];
}WIRE_TIMING_DATA;

typedef struct{
    int hasdata;
    int edgetype;
    float fall[3];
    float rise[3];
} CONSTRAINT_DATA;

typedef struct{
    char name[32];
    int instance_count;
    int numipins;
    char ipinname[MAX_IPINS][32];
    float ipinfallcap[MAX_IPINS];
    float ipinrisecap[MAX_IPINS];
    int numopins;
    char opinname[MAX_OPINS][32];
    int hasclkpin;
    char clkpinname[32];
    float clkpinfallcap;
    float clkpinrisecap;
    TIMING_DATA iotiming[MAX_IPINS][MAX_OPINS];
    TIMING_DATA cotiming[MAX_OPINS];
    CONSTRAINT_DATA setup[MAX_IPINS];
    CONSTRAINT_DATA hold[MAX_IPINS];
}P_CELL;

extern P_CELL *p_cell;

void parseCellLibrary();
#endif
