/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#ifndef _COMMANDLINE_H_
#define _COMMANDLINE_H_
#include <stdio.h>
// To make the command line arguments available to functions outside of main()

#endif
