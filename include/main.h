/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#ifndef _MAIN_H_
#define _MAIN_H_
#include <stdio.h>

    // Uncomment to enable logging
    //#define ENABLE_LOGGING
    #ifdef ENABLE_LOGGING
        extern FILE *main_logfile;
    #endif

    typedef struct
    {
        int netlist_parser_num_threads;
        char debug_message[200];
    } GlobalParams;

    extern GlobalParams global_params;

    extern char *netlistFile;
    extern char *cellFile;
    extern char *outFile;
    extern char *delayFile;
    extern char **envVars;
    extern int num_threads;

    extern FILE *out_file;
    extern FILE *delay_file;

    //#define SET_GLOBAL_PARAMS

    //#define ENABLE_TIMEIT
    #ifdef ENABLE_TIMEIT
        extern FILE *perf_logfile;
    #endif

    #define MAX_NUM_PARSER_THREADS 7
#endif
