/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#ifndef _READWHOLEFILE_H_
#define _READWHOLEFILE_H_

extern size_t bytes_read;
extern unsigned char * file_contents;
char* myfgets(char *line, int buffer_size);

#endif
