CFLAGS = -static-libgcc

final : ./bin/tau2014_converter

./bin/tau2014_converter : ./lib/libutils.a ./lib/liboutput.a ./install/commandline.o ./lib/libengine.a ./lib/libobjects.a ./lib/libparser.a ./src/test-parser.c
	@echo "Building the tau2014_converter"
	@gcc $(CFLAGS) -fopenmp ./src/test-parser.c ./install/output.o ./install/commandline.o -L ./lib -lm -lparser -lobjects -lengine -loutput -lpthread -lutils -I ./include -o ./bin/tau2014_converter
	@echo "Successful."

./lib/liboutput.a : ./src/output.c ./include/output.h ./src/lexicographic.c ./include/lexicographic.h
	@echo "Compiling lexicographic.c, output.c "
	@gcc $(CFLAGS) -c ./src/lexicographic.c  -I ./include -o ./install/lexicographic.o
	@gcc $(CFLAGS) -c ./src/output.c  -I ./include -o ./install/output.o
	@ar rcs ./lib/liboutput.a ./install/output.o ./install/lexicographic.o

./install/commandline.o :  ./src/commandline.c ./include/commandline.h
	@echo "Compiling commandline.c "
	@gcc $(CFLAGS) -c ./src/commandline.c  -I ./include -o ./install/commandline.o
	
./lib/libengine.a : ./src/engine.c ./include/engine.h ./src/vect.c ./include/vect.h ./lib/libobjects.a
	@echo "Compiling Engine Source"
	@gcc $(CFLAGS) -c ./src/vect.c -I ./include -o ./install/vect.o
	gcc $(CFLAGS) -c ./src/engine.c  -I ./include -o ./install/engine.o
	@echo "Building Engine Library"
	@ar rcs ./lib/libengine.a ./install/engine.o ./install/vect.o

./lib/libparser.a : ./src/parser_celllib.c  ./src/parser_netlist.c ./include/parser_celllib.h ./include/parser_netlist.h
	@echo "Building cell library parser"
	@gcc $(CFLAGS) -c ./src/parser_celllib.c  -I ./include -o ./install/parser_celllib.o
	@echo "Building netlist parser"
	@gcc $(CFLAGS) -c ./src/parser_netlist.c  -I ./include -o ./install/parser_netlist.o
	@echo "Building parser lib"
	@ar rcs ./lib/libparser.a ./install/parser_celllib.o  ./install/parser_netlist.o

./lib/libobjects.a : ./src/pins.c ./src/objects.c ./src/hash.c  ./include/pins.h ./include/objects.h ./include/hash.h
	@echo "Compiling Objects Library"
	@gcc $(CFLAGS) -c ./src/pins.c  -I ./include -o ./install/pins.o
	@gcc $(CFLAGS) -c ./src/objects.c  -I ./include -o ./install/objects.o
	@gcc $(CFLAGS) -c ./src/hash.c  -I ./include -o ./install/hash.o
	@echo "Building Objects Library"
	@ar rcs ./lib/libobjects.a ./install/pins.o ./install/objects.o ./install/hash.o

test : ./src/readwholefile_test.c ./install/readwholefile.o
	@gcc $(CFLAGS) ./src/readwholefile_test.c ./install/readwholefile.o  -L ./lib -lutils -I ./include -o ./bin/test-readwholefile.out	

./install/readwholefile.o : ./src/readwholefile.c ./include/readwholefile.h ./lib/libutils.a
	@echo "Compiling readwholefile.c"
	@gcc $(CFLAGS) -c ./src/readwholefile.c  -I ./include -o ./install/readwholefile.o

./lib/libutils.a : ./src/timeit.c ./include/timeit.h
	@gcc $(CFLAGS) -c ./src/timeit.c  -I ./include -o ./install/timeit.o
	@echo "Building Utils Library"
	@ar rcs ./lib/libutils.a ./install/timeit.o

timeit : ./src/timeit_test.c ./lib/libutils.a
	@gcc $(CFLAGS) ./src/timeit_test.c  -L ./lib/ -lutils -I ./include -o ./bin/test-timeit

threadfile : ./src/pthread_file.c
	@gcc $(CFLAGS) ./src/pthread_file.c  -lpthread -I ./include -o ./bin/test-pthread-file

openmp : ./src/openmp.c ./lib/libutils.a
	@gcc $(CFLAGS) -fopenmp ./src/openmp.c  -L ./lib/ -lutils -lm -I ./include -o ./bin/test-openmp

quicksort : ./src/quickSort.c
	@gcc -g ./src/quickSort.c  -I ./include -o ./bin/quickSort

mmap : ./src/test_mmap.c ./lib/libutils.a
	gcc $(CFLAGS) ./src/test_mmap.c -L ./lib/ -lutils -I ./include -o ./bin/mmap-profile.out

clean : 
	@echo "cleaning bin, lib, install directories"
	@rm ./bin/*.out ./install/*.o ./lib/*.a
