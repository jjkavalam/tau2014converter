#! /usr/bin/perl
use strict;
use warnings;

# Example usage
# cat mem_ctrl.net | perl scripts/resolve_direct_connection.pl x147076 > mem_ctrl.modified.net
# 
# The converter doesn't handle a direct connection between two instances (this 
# includes a primary input that directly connects to an instance) and issues
# a warning.
# In such cases, the direct connection should be removed and a zero-delay wire
# element be introduced.
# 
# This script automates this process of making necessary changes to the netlist
# provided the node name corresponding to the direct connection is passed as an
# argument
# 
if (scalar @ARGV != 1){
	print STDERR "Give node name as argument\n";
	exit ;
}
my $node = shift @ARGV;
print STDERR "Resolving direct connection at node $node \n";

# Grep for the nodename
# First match should be an "input" line
# If not, exit.
# Set count = 0
# for each matching line
# if line is "instance ..." then increment $count and replace $node with $node_$count
# end
# Prepare the dummy wire just before that
# Find first occurence of wire and split the file at that line and insert the dummy wire there
my $matchCount = 0;
my $fanOut = 0;
my $freeRun = 0;
while (<STDIN>)
{
	chomp $_;
	if ($freeRun == 0){
		if ($_ =~ m/wire/){
			# Put dummy wire
			print "wire $node";
			for(my $i = 1; $i <= $fanOut; $i++){
				print " ".$node."_".$i;
			}
			print "\n";
			for(my $i = 1; $i <= $fanOut; $i++){
				print "    res $node $node"."_".$i." 0.0\n";

			}
			$freeRun = 1;	
		} elsif ($_ =~ m/$node/){
			$matchCount++;
			if ($matchCount == 1){
				if (!($_ =~ m/input/)){
					print STDERR "The direct connection should be a primary input node\n";
					exit;
				}
			} else {
				if ($_ =~ m/instance/){
					$fanOut++;
					my $x = $node."_".$fanOut;
					$_ =~ s/$node/$x/;
				}
			}

		}
	}

	print "$_\n";

}
print STDERR "Done\n";
